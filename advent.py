#!/usr/bin/env python


# adventofcode.com/2020
class Advent:
    def __init__(self, filename):
        with open(filename) as f:
            self.input = f.read().splitlines()

    def day1(self):
        def part1():
            # find the pair in the input that sum to 2020.
            # keep a scoreboard of the inverse of each number and stop
            # when found.
            scoreboard = {}
            i = 1
            for num in [int(n) for n in self.input]:
                if num in scoreboard:
                    return num * scoreboard[num]
                scoreboard[2020 - num] = num
                i += 1
            return -1

        def part2():
            # find the triad in the input that sum to 2020
            sortedInput = sorted([int(n) for n in self.input])
            ninput = len(sortedInput)
            for i in range(ninput):
                for j in range(i+1, ninput):
                    for k in range(j+1, ninput):
                        s = sortedInput
                        total = s[i] + s[j] + s[k]
                        if total == 2020:
                            return s[i] * s[j] * s[k]
                        if total > 2020:
                            break
        return part1(), part2()

    def day2(self):
        # parse input lines of the format "{min}-{max} {letter}: password"
        # the prefix portion defines a ruleset for the password
        # return tuple for ruleset and password
        def parseline(line):
            rule_pw = line.split(":")
            rule = rule_pw[0].split(" ")
            rule[0] = [int(n) for n in rule[0].split("-")]
            return rule, rule_pw[1]

        def validator(fnvalid):
            total = 0
            for line in self.input:
                total += int(fnvalid(*parseline(line)))
            return total

        def part1():
            # return a count of the lines conforming to letter being in a
            # min-max range per the ruleset numerals.
            def vfn(rule, pw):
                letterCount = pw.count(rule[1])
                minmax = rule[0]
                return letterCount >= minmax[0] and letterCount <= minmax[1]
            return validator(vfn)

        def part2():
            # return a count of the lines conforming to letter being
            # in exactly one of the specified loca-locb locations using
            # order 1 indexing.
            def vfn(rule, pw):
                indexes = rule[0]
                isAtIndex = int(pw[indexes[0] - 1] == rule[1])
                isAtIndex += int(pw[indexes[1] - 1] == rule[1])
                return isAtIndex == 1
            return validator(vfn)

        return part1(), part2()

    def day3(self):
        class TreeMap:
            def __init__(self, lines, treechar="#"):
                def indices(str, char):
                    rv = []
                    for i in range(len(str)):
                        if str[i] == treechar:
                            rv += [i]
                    return rv

                trees = []
                for line in lines:
                    trees += [indices(line, treechar)]
                self.trees = trees
                self.columns = len(lines[0])

            def walkAndCountTrees(self, right, down):
                col = right
                row = down
                ntrees = 0
                while row < len(self.trees):
                    if col in self.trees[row]:
                        ntrees += 1
                    col += right
                    col %= self.columns
                    row += down
                return ntrees

        def part1():
            # count trees for single map direction
            return TreeMap(self.input).walkAndCountTrees(3, 1)

        def part2():
            # count trees for given map directions, multiply all
            # values together and return.
            treemap = TreeMap(self.input)
            rval = 1
            for paths in [[1, 1], [3, 1], [5, 1], [7, 1], [1, 2]]:
                rval *= treemap.walkAndCountTrees(*paths)
            return rval

        return part1(), part2()


if __name__ == "__main__":
    print "day1:", Advent("day1.input").day1()
    print "day2:", Advent("day2.input").day2()
    print "day3:", Advent("day3.input").day3()
