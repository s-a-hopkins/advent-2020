#!/usr/bin/env ruby

class Advent
  def initialize(input_file, linesep="\n")
    f = File.new(input_file)
    @input = f.readlines(linesep)
  end
  def each_inputline(&block)
    @input.each { |line|
      yield line.chomp
    }
  end
  def day4
    def countrecs(validfn)
      validrecs = 0
      @input.each { |rec|
        rechash = Hash.new(false)
        attrs = rec.split(/[\n ]/)
        attrs.each { |attr|
          fields = attr.split(":")
          rechash[fields[0]] = fields[1]
        }
        if validfn.call(rechash)
          validrecs += 1
        end
      }
      return validrecs
    end

    def part1
      valid = lambda { |rec|
        reqd = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
        reqd.each { |r|
          if rec[r] == false
            return false
          end
        }
        return true
      }
      return countrecs(valid)
    end

    def part2
      valid = lambda { |rec|
        byr_valid = lambda { |value|
          val = value.to_i
          return val >= 1920 && val <= 2002
        }
        iyr_valid = lambda { |value|
          val = value.to_i
          return val >= 2010 && val <= 2020
        }
        eyr_valid = lambda { |value|
          val = value.to_i
          return val >= 2020 && val <= 2030
        }
        hgt_valid = lambda { |value|
          # a number followed by cm or in
          suffidx = value.index(/\D/)
          if suffidx == nil
            return false
          end
          num = value[0..suffidx].to_i
          case value[suffidx..-1]
          when "cm"
            return num >= 150 && num <= 193
          when "in"
            return num >= 59 && num <= 76
          else
            return false
          end
        }
        hcl_valid = lambda { |value|
          return value.index(/^#[0-9a-f]{6}$/) == 0
        }
        ecl_valid = lambda { |value|
          colors = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
          return colors.include?(value)
        }
        pid_valid = lambda { |value|
          return value.index(/^[0-9]{9}$/) == 0
        }
        validators = {
          'byr' => byr_valid,
          'iyr' => iyr_valid,
          'eyr' => eyr_valid,
          'hgt' => hgt_valid,
          'hcl' => hcl_valid,
          'ecl' => ecl_valid,
          'pid' => pid_valid
        }
        validators.each { |name, vfn|
          value = rec[name]
          if value == false or vfn.call(value) == false
            return false
          end
        }
        return true
      }
      return countrecs(valid)
    end
    return part1, part2
  end

  def day5
    def bspwalk(str, dirspec)
      min = 0
      max = 2**str.length - 1
      str.each_char { |s|
        midpt = (min + max) / 2
        if s == dirspec[0]
          max = midpt
        elsif s == dirspec[1]
          min = midpt + 1
        else
          puts "bad dirspec #{s} in #{str}.  #{dirspec} allowed."
        end
      }
      return min
    end

    def test_bspwalk
      puts "bspwalk:"
      puts "#{bspwalk('BFFFBBF', 'FB')}, 70"
      puts "#{bspwalk('RRR', 'LR')}, 7"
      puts "#{bspwalk('FFFBBBF', 'FB')}, 14"
      puts "#{bspwalk('BBFFBBF', 'FB')}, 102"
      puts "#{bspwalk('RLL', 'LR')}, 4"
    end

    def bsp2seatid(seatspec)
      row = bspwalk(seatspec[0..6], "FB")
      col = bspwalk(seatspec[7..9], "LR")
      return row * 8 + col       
    end

    def test_bsp2seatid
      puts "bsp2seatid:"
      puts "#{bsp2seatid('BFFFBBFRRR')}, 567"
      puts "#{bsp2seatid('FFFBBBFRRR')}, 119"
      puts "#{bsp2seatid('BBFFBBFRLL')}, 820"
    end

    def part1
      # find highest seat id
      maxseatid = 0
      @input.each { |line|
        seatid = bsp2seatid(line)
        if seatid > maxseatid
          maxseatid = seatid
        end
      }
      return maxseatid
    end

    def part2
      # find the missing seat id in the list.  the plane is
      # full except a portion at the front and back, so we
      # must start the search from the first occupied seat.
      seats = []
      @input.each { |line|
        seats.push(bsp2seatid(line))
      }
      seats.sort!
      lastseat = seats[0]
      seats[1..-1].each { |seat|
        if seat != lastseat + 1
          break
        end
        lastseat = seat
      }
      return lastseat + 1
    end

    def run_tests
      test_bspwalk()
      test_bsp2seatid()
    end

    return part1, part2
  end

  def day6
    def part1
      # sum the union of each group's yes answers; anyone answers yes
      sum = 0
      @input.each { |group|
        group_answers = {}
        group.split("\n").each { |person|
          person.each_char { |ch|
            group_answers[ch] = true
          }
        }
        sum += group_answers.length
      }
      return sum
    end

    def part2
      # sum the intersection of each group's yes answers; everyone answers yes
      sum = 0
      @input.each { |group|
        group_answers = Hash.new(0)
        group = group.split("\n")
        group.each { |person|
          person.each_char { |ch|
            group_answers[ch] += 1
          }
        }
        group_answers.each { |_, v|
          if v == group.length
            sum += 1
          end
        }
      }
      return sum
    end

    return part1, part2
  end

  def day7
    def get_container_maps
      def each_container(&block)
        each_inputline { |line|
          bags = line.split(" contain ")
          container = bags[0].split(/ bags.*$/)[0]
          contains = {}
          bags[1].split(/ bags?[,.] ?/).each { |bagspec|
            m = /([0-9]+) (.*)$/.match(bagspec)
            contains[m[2]] = m[1].to_i if m && m.length == 3
          }
          yield container, contains
        }
      end

      map = Hash.new { |hash, key| hash[key] = {} }
      inverse_map = Hash.new { |hash, key| hash[key] = {} }
      each_container { |container, contains|
        contains.each { |bag, count|
          map[container][bag] = count
          inverse_map[bag][container] = count
        }
      }
      return map, inverse_map
    end

    def count_container_types(container_map, name)
      # count total number of types recursively in map under name
      def walk_and_score(scoreboard, map, key)
        return if ! map[key]
        map[key].each_key { |altkey|
          if scoreboard[altkey] == false
            scoreboard[altkey] = true
            walk_and_score(scoreboard, map, altkey)
          end
        }
        scoreboard.length
      end
      walk_and_score(Hash.new(false), container_map, name)
    end

    def part1(inverse_container_map)
      count_container_types(inverse_container_map, "shiny gold")
    end

    def count_container_total(container_map, name)
      def walk_and_count(map, key)
        return 0 if ! map[key]
        total = 0
        map[key].each { |altkey, count|
          total += (walk_and_count(map, altkey) + 1) * count
        }
        return total
      end
      walk_and_count(container_map, name)
    end

    def part2(container_map)
      count_container_total(container_map, "shiny gold")
    end

    map, inverse_map = get_container_maps
    return part1(inverse_map), part2(map)
  end

  class Instruction
    attr_accessor :opcode, :arg

    def initialize(line)
      inst = line.split
      @opcode = inst[0]
      @arg = 0
      case @opcode
      when "acc", "jmp", "nop"
        @arg = inst[1].to_i
      else
        puts "unknown instruction @opcode"
      end
    end
  end

  class Code
    attr_accessor :code

    def initialize(input)
      @code = input.map { |line| Instruction.new(line) }
    end

    def run_code
      accumulator = 0
      pc = 0
      loop_detect = Hash.new(false)
      while pc < @code.length && loop_detect[pc] == false
        loop_detect[pc] = true
        case @code[pc].opcode
        when "jmp"
          pc = pc + @code[pc].arg
          next
        when "acc"
          accumulator = accumulator + @code[pc].arg
        end
        pc += 1
      end
      return accumulator, pc
    end
  end    

  def day8
    def part1
      Code.new(@input).run_code[0]
    end

    def part2
      program = Code.new(@input)
      program.code.each_with_index { |instruction, pc|
        case instruction.opcode
        when "jmp"
          flip_opcode = "nop"
        when "nop"
          flip_opcode = "jmp"
        else
          next
        end
        orig_opcode = instruction.opcode
        instruction.opcode = flip_opcode
        acc, endpc = program.run_code
        instruction.opcode = orig_opcode
        return acc if endpc == program.code.length
      }
      0
    end
    return part1, part2
  end

  def day9
    def sumcheck?(numbers, numbers_hash, sum)
      numbers.each { |n|
        if numbers_hash[sum - n]
          return true
        end
      }
      return false
    end
    def part1
      last25 = Array.new(25, 0)
      last25h = Hash.new(false)
      i = 0
      # prime the lists
      until i == 25 do
        num = @input[i].to_i
        last25[i] = num
        last25h[num] = true
        i += 1
      end
      until i == @input.length do
        num = @input[i].to_i
        return num if !sumcheck?(last25, last25h, num)
        retire_num = last25[i%25]
        last25h[retire_num] = false
        last25[i%25] = num
        last25h[num] = true
        i += 1
      end
    end

    def part2
      def scan_min_max(arr)
        min = max = arr[0].to_i
        arr[1..-1].each { |n|
          n = n.to_i
          min = n if n < min
          max = n if n > max
        }
        return min, max
      end
      seeksum = 14360655
      start = 0
      stop = 1
      xsum = @input[0].to_i + @input[1].to_i
      until start == @input.length - 1
        if xsum == seeksum
          min, max = scan_min_max(@input[start..stop])
          return min + max
        elsif xsum > seeksum
          xsum -= @input[start].to_i
          start += 1
        elsif stop != @input.length - 1
          stop += 1
          xsum += @input[stop].to_i
        else
          break
        end
      end
      return "not found"
    end
    return part1, part2
  end

  def day10
    def part1
      ones = 0
      threes = 1 # built-in
      last = 0
      @input.map(&:to_i).sort.each { |num|
        num = num.to_i
        case num - last
        when 1
          ones += 1
        when 3
          threes += 1
        end
        last = num
      }
      return ones * threes
    end

    def part2
      count = 0
      sorted_nums = @input.map(&:to_i).sort
      def gap_bound(nums, index)
        return 0 if index < 0
        return nums[num.length-1] + 3 if index >= nums.length
        return nums[index]
      end
      i = 0
      until i == sorted_nums.length - 1
        num = sorted_nums[i]
        pregap = num - gap_bound(sorted_nums, i-1)
        postgap = gap_bound(sorted_nums, i+1) - num
        count += 1 if pregap + postgap <= 3
        i += 1
      end
      return count
    end
    return part1, part2
  end
end

puts " day4: #{Advent.new("day4.input", "\n\n").day4()}"
puts " day5: #{Advent.new("day5.input").day5()}"
puts " day6: #{Advent.new("day6.input", "\n\n").day6()}"
puts " day7: #{Advent.new("day7.input").day7()}"
puts " day8: #{Advent.new("day8.input").day8()}"
puts " day9: #{Advent.new("day9.input").day9()}"
puts "day10: #{Advent.new("day10.input").day10()}"

